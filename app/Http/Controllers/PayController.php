<?php

namespace App\Http\Controllers;

use App\Models\pay;
use Illuminate\Http\Request;

class PayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('pays.index', [
           "pays" => pay::all()
       ]);
       
       
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    #
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "libelle" => 'required',
            "monnaie" => 'required',
            "capitale" => 'required'
        ]);

        pay::create([


            "libelle"=>$request->get('libelle'),
            "description"=>$request->get('description'),
            "code_indicatif"=>$request->get('code_indicatif'),
            "continent"=>$request->get('continent'),
            "population"=>$request->get('population'),
            "capitale"=>$request->get('capitale'),
            "monnaie"=>$request->get('monnaie'),
            "langue"=>$request->get('langue'),
            "superficie"=>$request->get('superfice'),
            "est_laique"=>$request->get('est_laique'),


        ]);
        return redirect()->route("pays.store");
        //
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function show(pay $pay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function edit(pay $pay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pay $pay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function destroy(pay $pay)
    {
        //
    }
}
