@extends("layouts.main")

@section('content')
<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Simple Table</h4>
                  <p class="card-category"> Here is a subtitle for this table</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <tr><th>
                          ID
                        </th>
                        <th>
                          Name
                        </th>
                        <th>
                          Country
                        </th>
                        <th>
                          City
                        </th>
                        <th>
                          Salary
                        </th>
                      </tr></thead>
                      <tbody>
                      @foreach($pays as $item)
                        <tr>
                          <td>
                            {{$item-> id}}
                          </td>
                          <td>
                            Dakota Rice
                            {{$item-> libelle}}
                          </td>
                          <td>
                            Niger
                            {{$item-> description}}
                          </td>
                          <td>
                            Oud-Turnhout
                            {{$item-> code_indicatif}}
                          </td>
                          <td>
                          {{$item-> continent}}
                          </td>
                          <td>
                          {{$item-> population}}
                          </td>
                          <td>
                          {{$item-> capitale}}
                          </td>
                          <td>
                          {{$item-> monnaie}}
                          </td>
                          <td>
                          {{$item-> langue}}
                          </td>
                          <td>
                          {{$item-> superficie}}
                          </td>
                          <td>
                          {{$item-> est_laique}}
                          </td>
                          <td class="text-primary">
                            $36,738
                          </td>
                        </tr>
                        
                        @endforeach
                      </tbody>
                    </table>
                    <a href="{{url('/create')}}"><button class="btn btn-primary">ajouter</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endsection