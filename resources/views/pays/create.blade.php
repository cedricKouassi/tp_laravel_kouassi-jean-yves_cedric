@extends('layouts.main')

@section('content')

<div class="col-md-8">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Edit Profile</h4>
        <p class="card-category">Complete your profile</p>
      </div>
      <div class="card-body">
        <form action="{{route('pays.store')}}" method="POST">
            @method("POST")
            @csrf
          <div class="row">
            <div class="col-md-5">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Libelle</label>
                <input type="text" name="libelle" class="form-control" >
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Description</label>
                <input type="text" name="description" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Code indicatif</label>
                <input type="text" name="code_indicatif" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group bmd-form-group">

                <select name="continent" class="form-control">
                    <option value="0">Continent :</option>
                    <option value="Afrique">Afrique</option>
                    <option value="Europe">Europe</option>
                    <option value="Amerique">Amerique</option>
                    <option value="Asie">Asie</option>
                    <option value="Oceanie">Oceanie</option>
                    <option value="Antartique">Antartique</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Population</label>
                <input type="number" name="population" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Capitale</label>
                <input type="text" name="capitale" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group bmd-form-group">


                        <select name="monnaie" class="form-control">
                            <option value="0">Monnaie :</option>
                            <option value="EUR">EUR</option>
                            <option value="XOF">XOF</option>
                            <option value="DOLLAR">DOLLAR</option>
                        </select>

              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">

                <select name="langue" class="form-control">
                    <option value="0">Langue :</option>
                    <option value="FR">FR</option>
                    <option value="EN">EN</option>
                    <option value="AR">AR</option>
                    <option value="ES">ES</option>
                </select>

              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Superficie</label>
                <input type="number" name="superficie" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
                <div class="form-group bmd-form-group">

                  <select name="est_laique" class="form-control">
                    <option value="est_laique ">Est Laique :</option>
                    <option value="1">Oui</option>
                    <option value="0">Non</option>
                </select>

                </div>
              </div>

          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">


              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary pull-right">Valider</button>

        </form>
      </div>
    </div>
  </div>

@endsection
